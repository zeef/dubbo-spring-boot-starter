package com.alibaba.dubbo.spring.boot.domain;

/**
 * 常量定义
 *
 * @author xionghui
 * @version 1.0.0
 * @since 1.0.0
 */
public class DubboSpringBootStarterConstants {

  public static final String GROUP = "group";

  public static final String VERSION = "version";
  
  public static final String TYPE = "type";

  public static final String NAME = "name";
  
  public static final String fNAME = "sname";
  
}
